#include <stdio.h>

int main() {
    double id32, fail;
    int scan;
    scan = scanf("%lf", &id32);
    if (id32 == 4294967295) {
        fail = 413;
        printf("%lf", fail);
    }
    else {
        printf("%lf", id32);
    }
    return 0;
}
