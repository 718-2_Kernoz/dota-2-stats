import os
import random

limit_low = 1294967295
hidden = 4294967295
limit_high = 123765248

Ids = (limit_high,  hidden)

for id32 in Ids:
    cmd = 'echo "{}\n" |  ./a.out'.format(id32)
    actual = os.popen(cmd).read().strip()
    if id32 == 4294967295:
        excepted = 413
    else:
        excepted = id32
        
    assert excepted == round(float(actual)), "{} == {}".format(excepted, actual)
