<?php
require 'steamauth/steamauth.php';
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <title>DotaStats</title>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="index, follow">
    <meta name="google" content="notranslate">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="">
    <style>
        <?php include './src/assets/scss/style.scss'; ?>
    </style>
</head>
<body>
    <div class="background">
        <video width=100% loop="loop" autoplay="">
            <source src="./src/assets/images/backgroubnd.mp4">
            <source src="./src/assets/images/backgroubnd.webm">
        </video>
    </div>
    <header class="header">
        <div class="header__navigation">
            <nav class="nav">
                <ul class ="nav__list">
                    <li class="nav__item">
                        <a class="nav__link" href="index.php">Main</a>
                    </li>
                    <li class="nav__item">
                        <a class="nav__link" href="about.php">About</a>
                    </li>
                    <li class="nav__item">
                        <a class="nav__link" href="https://www.dota2.com/patches/">Updates</a>
                    </li>
                    <?php 
                        if(isset($_SESSION['steamid'])){
                            echo '<div class="header__navigation">
                                        <nav class="nav">
                                            <ul class ="nav__list">
                                                <li class="nav__item">
                                                    <a class="nav__link" href="stats.php">Stats</a>
                                                </li>';
                        }
                    ?>
                </ul>
            </nav>
        </div>
        <div class="header__login">  
                <ul class="nav__log">
                    <ul class = "nav__login">
                        <?php
                            if(!isset($_SESSION['steamid'])) {
                            loginbutton(); //login button
                            }  
                            else {
                                include ('steamauth/userInfo.php'); //To access the $steamprofile array
                                //Protected content 
                                echo $steamprofile['personaname'];
                                $a = $steamprofile['avatarmedium'];
                                echo "<img src=$a>";
                                logoutbutton(); //Logout Button
                            }     
                        ?>
                    </ul>
                </ul>
        </div>
    </header>
    <div class="block_info">
        <div class="post">
        <div class="post_content">
            <p class="post__title">
                The Mistwoods Update
            </p>
            <img class="post_preview" src="src/assets/images/update1.jpg">
            <p class="post_text">
Глубоко в дальноземье, за пустынями Нанарака и к северу от угольных пожаров Кримвола, нас ждет коварное лесное пространство — где был обнаружен тайник с драгоценными осколками Аганима, и хитрый новый герой одичал.                <br>
                Как мы уже упоминали в предыдущем обновлении Dota Plus, новый сезон начнется 1 декабря и будет включать в себя сезонный сброс квестов, новое сезонное сокровище и многое другое. Мы также начали вносить изменения в бэкэнд для сбора данных для предстоящих функций Overwatch. Когда этот инструмент будет полностью реализован, он позволит пользователям просматривать подозрительные совпадения и поможет идентифицировать плохих игроков в дополнение к нашим существующим системам.
                <br>
Зайдите на страницу обновления Миствудов, чтобы узнать все об этом новом воине в битве за древних и о сокровищнице совершенно новых сил, которыми великодушие Аганима одарило всех нас.
            </p>
        </div>
    </div>
    <div class="post">
        <div class="post_content">
            <p class="post__title">
                Upcoming Updates
            </p>
            <img class="post_preview" src="src/assets/images/Tinker.jpg">
            <p class="post_text">
                Поскольку День Благодарения уже не за горами, мы хотели бы воспользоваться этой возможностью, чтобы поблагодарить вас всех за Вашу постоянную поддержку в течение всего года и пожелать вам оставаться в безопасности и здоровыми в течение этого времени. Кроме того, мы хотели бы дать вам еще одну быструю информацию о некоторых предстоящих вещах, над которыми мы работаем.
                <br>
                Как мы уже упоминали в предыдущем обновлении Dota Plus, новый сезон начнется 1 декабря и будет включать в себя сезонный сброс квестов, новое сезонное сокровище и многое другое. Мы также начали вносить изменения в бэкэнд для сбора данных для предстоящих функций Overwatch. Когда этот инструмент будет полностью реализован, он позволит пользователям просматривать подозрительные совпадения и поможет идентифицировать плохих игроков в дополнение к нашим существующим системам.
                <br>
                На игровом фронте наш первоначальный план для нового героя был 30 ноября, однако ей нужно немного больше времени, чтобы уютно устроиться на зиму. Мы выпустим ее вместе с обновлением геймплея 7.28 в середине декабря.
                <br>
                Наконец мы рады сообщить, что Dota 2021 года. Dota Pro Circuit стартует 18 января, и 16 команд в двух дивизионах будут соревноваться в каждой из шести региональных лиг. Лучшие из каждого региона также будут претендовать на первый мейджор сезона. Более подробная информация о расписании и деталях будет доступна в ближайшее время.
            </p>
        </div>
    </div>
    <div class="post">
        <div class="post_content">
            <p class="post__title">
                Dota Plus Update Winter 2020
            </p>
            <img class="post_preview" src="src/assets/images/plus_winter2020_update_blog.jpg">
            <p class="post_text">
                Зимний мрак может омрачить земли, но для тех, кто сражается за господство над путями и разрушение древнего, ничто не останавливает действие. А для самых преданных, чье мастерство уже не вызывает сомнений, сегодняшнее обновление открывает новый уровень величия-гроссмейстер-наряду с другими сезонными обновлениями для Dota Plus.
                <br>
                Уровень Гроссмейстера Dota Plus
                Участники Dota Plus теперь могут повышать уровень героев до совершенно нового уровня гроссмейстера на уровне 30. В дополнение к врожденному фактору запугивания, игроки, получившие титул гроссмейстера, имеют доступ к новой голосовой линии колеса чата для этого героя. Любые испытания героя, выполненные в качестве гроссмейстера,также дадут осколки в качестве награды вместо опыта героя. 
            </p>
        </div>
    </div>
</div>
<div class="block_blur">
    <div class="post">
        <div class="post_content">
            <p class="post__title">
                Вход
            </p>
            <img class="post_preview" src="src/assets/images/blur.jpg">
            <p class="post_text">
                Для того чтобы узнать свою статистику матчей, необходимо авторизироваться через профиль Steam, сделать это можно нажав на кнопку Войти.
            </p>
        </div>
    </div> 
</div>
</body>
</html>
