<?php
require 'steamauth/steamauth.php';
require_once '../../vendor/autoload.php';
use Dota2Api\Api;
Api::init('13208987BD878CA99A235C79B2430FFC', array('localhost', 'root', '', 'dota2', ''));
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <title>DotaStats</title>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="index, follow">
    <meta name="google" content="notranslate">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="">
    <style>
        <?php include './src/assets/scss/style.scss'; ?>
    </style>
</head>
<body>
    <div class="background">
        <video width=100% loop="loop" autoplay="">
            <source src="./src/assets/images/backgroubnd.mp4">
            <source src="./src/assets/images/backgroubnd.webm">
        </video>
    </div>
    <header class="header">
        <div class="header__navigation">
            <nav class="nav">
                <ul class ="nav__list">
                    <li class="nav__item">
                        <a class="nav__link" href="index.php">Main</a>
                    </li>
                    <li class="nav__item">
                        <a class="nav__link" href="about.php">About</a>
                    </li>
                    <li class="nav__item">
                        <a class="nav__link" href="https://www.dota2.com/patches/">Updates</a>
                    </li>
                    <?php 
                        if(isset($_SESSION['steamid'])){
                            echo '<div class="header__navigation">
                                        <nav class="nav">
                                            <ul class ="nav__list">
                                                <li class="nav__item">
                                                    <a class="nav__link" href="stats.php">Stats</a>
                                                </li>';
                        }
                    ?>
                </ul>
            </nav>
        </div>
        <div class="header__login">  
                <ul class="nav__log">
                    <ul class = "nav__login">
                        <?php
                            if(!isset($_SESSION['steamid'])) {
                            loginbutton(); //login button
                            }  
                            else {
                                include ('steamauth/userInfo.php'); //To access the $steamprofile array
                                $a = $steamprofile['avatarmedium'];
                                echo "<img src=$a>";
                            }     
                        ?>
                    </ul>
                    <ul class = "nav__login">
                        <?php
                            if(isset($_SESSION['steamid'])) {
                            include ('steamauth/userInfo.php'); //To access the $steamprofile array
                                //Protected content
                                echo $steamprofile['personaname'];
                                logoutbutton(); //Logout Button
                            }
                        ?>
                    </ul>
                </ul>
        </div>
    </header>
    <div class="block_about">
    <div class="post">
        <div class="post_content">
            <?php
                $playersMapperWeb = new Dota2Api\Mappers\PlayersMapperWeb();
                $playersInfo = $playersMapperWeb->addId($_SESSION['steamid'])->load();
                foreach($playersInfo as $playerInfo) {
                    $a = $steamprofile['avatarfull'];
                    $Convertid = new Dota2Api\Models\Player();
                    $id32 = $Convertid->convertId($_SESSION['steamid']);
                    $url = 'https://ru.dotabuff.com/players/'.$id32;
                    $curl_handle=curl_init();
                    $agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36';
                    curl_setopt($curl_handle, CURLOPT_URL,$url);
                    curl_setopt($curl_handle, CURLOPT_USERAGENT, $agent);
                    curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
                    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
                    $query = curl_exec($curl_handle);
                    curl_close($curl_handle);

                    $first_step = explode( '<span class="wins">' , $query );
                    $second_step = explode("</span>" , $first_step[1] );
                    $wins = $second_step[0];
                    $first_step = explode( '<span class="losses">' , $query );
                    $second_step = explode("</span>" , $first_step[1] );
                    $loses = $second_step[0];
                    $first_step = explode( '<span class="abandons">' , $query );
                    $second_step = explode("</span>" , $first_step[1] );
                    $abandons = $second_step[0];
                    $first_step = explode( '<div class="rank-tier-images">' , $query );
                    $second_step = explode("</div>" , $first_step[1] );
                    $rank = $second_step[0];
                    
                    echo"<div class='profile_content'>"
                    . "<div class='profile_pic'>";
                    echo "<img src=$a>"; 
                    echo "</div>";
                    
                    echo "<div class='profile_name'>"
                    . "<p class='post_text'>";
                    echo $steamprofile['personaname']."</p><p class='post_text'>";
                    echo '<a href="'.$playerInfo->get('profileurl').'">'.$playerInfo->get('personaname').'\'s steam profile</a>';
                    echo "</p>";
                    
                    echo "<p class= 'win'> Победы ".$wins. "</p>";
                    echo "<p class= 'defeat'> Поражения ".$loses. "</p>";
                    echo "<p class= 'post_text'> Покинутые игры ".$abandons. "</p>";
                    echo"</div>";
                    
                    echo "<div class='profile_rank'>";
                    echo  $rank;
                    echo "</div>";
                    echo"</div>";
                    
                    
                   
                }
                
            ?>
        </div>
    </div>
    </div>
            <?php
                $i = 0;
                $matchesMapperWeb = new Dota2Api\Mappers\MatchesMapperWeb();
                $matchesMapperWeb->setAccountId($_SESSION['steamid']);
                $matchesShortInfo = $matchesMapperWeb->load();
                foreach ($matchesShortInfo as $key=>$matchShortInfo) {
                    $matchMapper = new Dota2Api\Mappers\MatchMapperWeb($key);
                    $match = $matchMapper->load();
                    $ns = $match->getDataArray();           //Принимаем информацию о матче         
                    $time = $ns['duration'];
                    $min = floor($time/60);
                    $sec = round(($time/60 - $min)*60);
                    $mm = new Dota2Api\Mappers\MatchMapperWeb($ns['match_id']);
                    $match_info = $mm->load();
                    $Convertid = new Dota2Api\Models\Player();
                    $id32 = $Convertid->convertId($_SESSION['steamid']);
                    $find = $match_info->getAllSlots();
                    for ($s=0; $s<133; $s++){
                        $slot = $match_info->getSlot($s)->getDataArray();

                        if($slot['account_id'] == $id32){
                            break;
                        }
                        if($s==4){
                            $s=127;
                        }
                    }
                    $unit_match = $match_info->getSlot($s)->getDataArray();
                    $heroes = new Dota2Api\Data\Heroes();
                    $heroes->parse();
                    $heroId =$unit_match['hero_id'];
                    $pic = $heroes->getImgUrlById($heroId, false);
                    $name = $heroes->getDataById($heroId)['localized_name'];
                    $date = $match_info->getDataArray()['start_time'];
                    $result = $match_info->getDataArray()['radiant_win'];
                    $kills = $unit_match['kills'];
                    $deaths = $unit_match['deaths'];
                    $assists = $unit_match['assists'];
                    if($unit_match['player_slot']<6){
                        $team = 0;
                    }
                    else if($unit_match['player_slot']>127){
                        $team = 1;
                    }
                    if($team == 0 & $result ==0){
                        $status_match = 'Поражение';
                        $color = '61170D';
                    }
                    else if($team == 0 & $result ==1){
                        $status_match = 'Победа';
                        $color = '145723';
                    }
                    else if($team == 1 & $result ==0){
                        $status_match = 'Победа';
                        $color = '145723';
                    }
                    else if($team == 1 & $result ==1){
                        $status_match = 'Поражение';
                        $color = '61170D';
                    }
                    echo "<div class='block_mini_match'>"
                            . "<div class='block_id_match'>"
                    . "<p class='post_text'>";
                    echo $ns['match_id'];
                    echo "</p>"
                    . "</div>"
                    . "<div class='block_pic'>"
                            . "<img src=$pic height=64px>"
                            . "</div>"
                            . "<div class='block_name'>"
                            . "<p  class='post_text'>";
                    echo $name,
                            "</p>"
                            . "</div>";
                    
                    echo "<div class ='block_time'>"
                    . "<p class='post_text'>",
                            "Time: ",
                            "</p>"
                    . "<p class='post_text'>",
                            $min,":",$sec,
                            "</p>"
                            . "</div>";
                    
                    echo "<div class ='block_kda'>"
                    . "<p class='post_text'>",
                            "K | D | A",
                            "</p>"
                    . "<p class='post_text'>",
                            $kills,"|",$deaths,"|",$assists,
                            "</p>"
                            . "</div>";
                    
                    
                    
                    echo "<div class='block_date'>";
                    if($status_match == "Поражение"){
                        echo "<p class='defeat'>";}
                    else{
                        echo "<p class='win'>";}
                    echo $status_match,
                             "</p>"
                    . "<p class='post_text'>",$date,"</p>"
                            . "</div>";
                    echo "</div>";     //Конец блока мини матча
                             

                    
                    $i = $i +1;
                    if($i == 20){
                        break;
                    }
                }
                echo "<div class='block_mini_match'>"
                . "<p class='post_text'>";
                print_r($heroes);
                        echo"</p></div>";
            ?>
        
</body>
</html>
